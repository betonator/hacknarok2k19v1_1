﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstDestroy : MonoBehaviour
{
    [SerializeField]
    GameObject particlePrefab;
    [SerializeField]
    public Transform attachNode;

    public int obstacleHP = 25;
    public int damage = 5;
    public float killTime = 3f;

    public void LoseHP(int amount)
    {
        obstacleHP -= amount;
        if(obstacleHP <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        GameObject particleInstance = Instantiate(particlePrefab);
        particleInstance.GetComponent<ParticleSystem>().Play();
        particleInstance.transform.position = transform.position;
        particleInstance.transform.SetParent(transform.parent);
        Destroy(particleInstance, killTime);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet2"))
        {
            Destroy(other.gameObject);
            LoseHP(other.GetComponent<Bullet>().damage);
        }
        else if (other.CompareTag("Player"))
        {
            other.GetComponentInParent<Player>().LoseHP(damage);
            Die();
        }
    }


}
