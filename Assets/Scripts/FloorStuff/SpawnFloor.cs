﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFloor : MonoBehaviour
{

    public int forwardFloorCount = 5;

    public float FloorMoveSpeed = 22f;

    Score scoreScript;
    public GameObject scoreHandler;

    public GameObject FloorOBJ;

    float baseSpeed;
    public GameObject Floor1;
    public GameObject Floor2;
    public GameObject Floor3;

    public float MULTIPLY = 100;

    private GameObject attachPoint;
    private GameObject NewFloor;
    private GameObject spawnPoint;

    
    
    // Start is called before the first frame update
    void Start()
    {
        scoreScript = (Score) scoreHandler.GetComponent<Score>();
        NewFloor = transform.GetChild(0).gameObject;

        baseSpeed = FloorMoveSpeed;

        attachPoint = NewFloor.transform.GetChild(0).gameObject;
        spawnPoint = NewFloor.transform.GetChild(1).gameObject;
        
        


        for (int i = 0; i < forwardFloorCount;i++)
        {
            SpawnNextFC();
        }
               

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Score: " + scoreScript.score_float);
        FloorMoveSpeed = baseSpeed + scoreScript.score_float / MULTIPLY;
    }

    public void SpawnNextFC()
    {

        Vector3 spawnPos;
        Vector3 Dist = NewFloor.transform.position - spawnPoint.transform.position;


        spawnPos = attachPoint.transform.position + Dist;


        System.Random rnd = new System.Random();
        int k = rnd.Next(0, 3);

        NewFloor = Instantiate(Floor3, spawnPos, Floor3.transform.rotation);


       /* if (k==0)
        {
            NewFloor = Instantiate(Floor1, spawnPos, Floor1.transform.rotation);
            
        }
        else if(k == 1)
        {
            NewFloor = Instantiate(Floor2, spawnPos, Floor2.transform.rotation);
        }
        else
        {
           
            NewFloor = Instantiate(Floor3, spawnPos, Floor3.transform.rotation);
        }*/

        


        NewFloor.transform.SetParent(transform);
        attachPoint = NewFloor.transform.GetChild(0).gameObject;
        spawnPoint = NewFloor.transform.GetChild(1).gameObject;


    }
}
