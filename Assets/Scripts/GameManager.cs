﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    private GameObject gameOverScreen;
    [SerializeField]
    private GameObject highScoresScreen;
    [SerializeField]
    private Score scoreCounter;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private InputField nameinputField;
    [SerializeField]
    private ScoreBoard scoreBoard;
    [SerializeField]
    private EnemyGenerator enemyGenerator;

    private bool gameOverPanelActive = false;
    private bool highScorePanelActive = false;
    int score;
    string playerName;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        Cursor.visible = false;
        gameOverScreen.SetActive(gameOverPanelActive);
        highScoresScreen.SetActive(highScorePanelActive);
        foreach (Enemy enemy in enemyGenerator.enemiesList)
        {
            enemy.Die();
        }
        EnemyGenerator.enemyNumber = 0;
    }

    public void GameOver()
    {
        Cursor.visible = true;
        score = (int)scoreCounter.score_float;
        scoreCounter.enabled = false;
        GameOverScreenToggle();
        scoreText.text = "Your score: " + score;
        Time.timeScale = 0;
    }

    public void Submit()
    {
        playerName = nameinputField.text;
        scoreBoard.userName = playerName;
        scoreBoard.score = score;
        GameOverScreenToggle();
        HighScoreScreenToggle();

    }

    public void GameOverScreenToggle()
    {
        gameOverPanelActive = !gameOverPanelActive;
        gameOverScreen.SetActive(gameOverPanelActive);
    }

    public void HighScoreScreenToggle()
    {
        highScorePanelActive = !highScorePanelActive;
        highScoresScreen.SetActive(highScorePanelActive);
    }

    public void ReloadGame()
    {
        Cursor.visible = false;
        HighScoreScreenToggle();
        scoreCounter.enabled = true;
        scoreCounter.score_float = 0;
        Time.timeScale = 1;
        SceneManager.LoadScene("Game");
    }
}
