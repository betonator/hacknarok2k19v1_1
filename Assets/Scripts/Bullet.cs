﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage = 10;
    public string tag;

    private void Start()
    {
        tag = transform.tag;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(tag == "EnemyBullet")
            {
                other.GetComponent<Player>().LoseHP(damage);
                Destroy(this.gameObject);
            }
        }
        if(other.CompareTag("Enemy"))
        {
            if(tag == "CombinedBullet")
            {
                other.GetComponent<Enemy>().Die();
            }
            else if (tag == "Bullet1")
            {
                Debug.Log("Enemy hit by player bullet");
                other.GetComponent<Enemy>().LoseHP(damage);
                Destroy(this.gameObject);
            }
        }
        if (other.CompareTag("Obstacle"))
        {
            if (tag == "CombinedBullet")
            {
                other.GetComponent<ObstDestroy>().Die();
            }
        }
    }
}
