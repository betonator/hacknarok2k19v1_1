﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public float score_float = 0;
    public Text current_score;

    public float multiplier = (1000 / 60);

    // Update is called once per frame
    void Update()
    {
        score_float += Time.deltaTime*multiplier;
        current_score.text = "Score: " + score_float.ToString("0");

    }
}
