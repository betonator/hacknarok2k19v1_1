﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersFormHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject player1Prefab;
    [SerializeField]
    private GameObject player2Prefab;
    [SerializeField]
    private GameObject mergedPlayerPrefab;

    [SerializeField]
    GameObject currentPlayer1;
    [SerializeField]
    GameObject currentPlayer2;
    GameObject currentMergedPlayer;

    private bool playersMerged = false;

    private float mergeDisablerTimer;
    public float mergeDisablerInterval = 3f;

    private float mergedTimer;
    public float mergedDuration = 10f;

    [SerializeField]
    private DualCamera dualCamera;

    private void ActualizeCamera(GameObject object1, GameObject object2)
    {
        dualCamera.player1 = object1;
        dualCamera.player2 = object2;
    }

    private void Start()
    {
        mergeDisablerTimer = mergeDisablerInterval;
        currentMergedPlayer = null;
    }

    private void Update()
    {
        mergedTimer -= Time.deltaTime;
        mergeDisablerTimer -= Time.deltaTime;
        if (Input.GetAxis("Merge1") == 1.0f && Input.GetAxis("Merge2") == 1.0f)
        {
            if (!playersMerged)
            {
                Vector3 distance = currentPlayer1.transform.position - currentPlayer2.transform.position;
                if (distance.magnitude <= 3)
                {
                    if (mergeDisablerTimer <= 0.0f)
                    {
                        mergeDisablerTimer = mergeDisablerInterval;
                        playersMerged = !playersMerged;
                        currentMergedPlayer = Instantiate(mergedPlayerPrefab);
                        mergedPlayerPrefab.GetComponent<CombinedPlayer>().health = currentPlayer1.GetComponent<Player>().health + currentPlayer2.GetComponent<Player>().health;
                        currentMergedPlayer.transform.position = (currentPlayer1.transform.position + currentPlayer2.transform.position) / 2;
                        Destroy(currentPlayer2);
                        Destroy(currentPlayer1);
                        ActualizeCamera(currentMergedPlayer, currentMergedPlayer);
                        mergedTimer = mergedDuration;
                    }
                }
            }
           
        }
        else if(playersMerged && mergedTimer <= 0.0f)
        {
            mergeDisablerTimer = mergeDisablerInterval;
            playersMerged = !playersMerged;
            currentPlayer1 = Instantiate(player1Prefab);
            currentPlayer2 = Instantiate(player2Prefab);
            currentPlayer1.GetComponent<Player>().health = currentMergedPlayer.GetComponent<CombinedPlayer>().health / 2;
            currentPlayer2.GetComponent<Player>().health = currentMergedPlayer.GetComponent<CombinedPlayer>().health / 2;
            currentPlayer1.transform.position = currentMergedPlayer.transform.position + new Vector3(-1, 0.0f, 0.0f);
            currentPlayer2.transform.position = currentMergedPlayer.transform.position + new Vector3(1, 0.0f, 0.0f);
            Destroy(currentMergedPlayer);
            ActualizeCamera(currentPlayer1, currentPlayer2);
        }
    }
}
