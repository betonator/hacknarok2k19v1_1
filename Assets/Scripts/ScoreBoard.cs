﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    public Text score_list;
    public static int[] best_scores =new int[8];
    private static string[] names = new string[8];

    private static int counter = 0;

    public int score = 0;
    public string userName;

    void Start()
    {
        if (counter == 0)
        {
            best_scores[0] = 10;
            names[0] = "Adrian";

            best_scores[1] = 9;
            names[1] = "Bartek ";

            best_scores[2] = 8;
            names[2] = "Celinka ";

            best_scores[3] = 7;
            names[3] = "Dominik ";

            best_scores[4] = 6;
            names[4] = "Ewa ";

            best_scores[5] = 5;
            names[5] = "Franek ";

            best_scores[6] = 4;
            names[6] = "Gosia ";

            best_scores[7] = 3;
            names[7] = "Hanka ";


            for (int i = 1; i <= 8; i++)
            {
                score_list.text = i.ToString() + ". " + names[i - 1] + " " + best_scores[i - 1].ToString() + "\n";
            }
            counter++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // GET LAST SCORE PLAYER HAD BEFORE HE DIED!!!

        bool updated = false;

        if (score > best_scores[7])
        {
            score_list.text = "";

            updated = true;
            best_scores[7] = score;
            names[7] = userName + ": ";       // GET PLAYER NAME
            score = 0;
        

            int i = 6;
            while (i >= 0)
            {
                if (best_scores[i] < best_scores[i + 1])
                {
                    int temp_1 = best_scores[i+1];
                    string temp_2 = names[i+1];

                    best_scores[i+1] = best_scores[i];
                    names[i+1] = names[i];
                    
                    best_scores[i] = temp_1;
                    names[i] = temp_2;
                }
                i--;
            }
        }

        if (updated)
        {
            //score_list.text = "Best Scores" + "\n";
            for (int i = 1; i <= 8; i++)
            {
                score_list.text = score_list.text + i.ToString() + ". " + names[i-1] + " " + best_scores[i-1].ToString() + "\n";
            }

        }
    }   // End of void Update()








}
    
    

