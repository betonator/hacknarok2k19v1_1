﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinedPlayer : MonoBehaviour
{
    public int health;

    public float speed = 10;
    public float hoverHeight = 3.5f;
    public float hoverForce = 60.0f;
    public float rotationSpeed = 2.0f;
    private float shootingTimer;
    public float shootingInterval = 1.0f;
    private float dodgeTimer;
    private float dodgeDuration;
    private float currentDodgeTime = 0;
    public float dodgeInterval = 2.0f;
    public float dodgeLength = 4.0f;
    public float dodgeSpeed = 30;
    private Rigidbody playerBody;

    [SerializeField]
    private Transform gunPlacedPosition;
    [SerializeField]
    private Transform gunShootAtPosition;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    public float bulletSpeed = 10f;

    private void Start()
    {
        dodgeDuration = dodgeLength / dodgeSpeed;
        shootingTimer = shootingInterval;
        dodgeTimer = dodgeInterval;
        playerBody = GetComponent<Rigidbody>();
        currentDodgeTime = 0;
    }

    public State state;
    public enum State
    {
        Normal,
        Dodging,
    }

    private void Update()
    {
        switch (state)
        {
            case State.Normal:
                PlayerMovement();
                PlayerShoot();
                CheckPlayerDodge();
                break;
        }
    }

    private void FixedUpdate()
    {
        if (state == State.Dodging)
        {
            Dodge();
        }
        HoverMotor();
    }

    private void PlayerMovement()
    {
        float horizontal = Input.GetAxis("Horizontal1") + Input.GetAxis("Horizontal2");
        float vertical = Input.GetAxis("Vertical1") + Input.GetAxis("Vertical2");
        Vector3 axisMovement = new Vector3(horizontal * Time.deltaTime / 2, 0.0f, vertical * Time.deltaTime / 2);
        axisMovement.Normalize();
        axisMovement *= speed;
        Vector3 movement = new Vector3(axisMovement.x, playerBody.velocity.y, axisMovement.z);
        playerBody.velocity = movement;
        //turning
        transform.rotation = Quaternion.RotateTowards(transform.rotation,
            Quaternion.Euler(0.0f, 0.0f, -playerBody.velocity.x / 2),
            rotationSpeed * Time.deltaTime);
    }

    private void CheckPlayerDodge()
    {
        dodgeTimer -= Time.deltaTime;
        if (Input.GetAxis("Dodge1") > 0.0f && Input.GetAxis("Dodge2") > 0.0f)
        {
            if (dodgeTimer <= 0.0f)
            {
                dodgeTimer = dodgeInterval;
                state = State.Dodging;
            }
        }
    }

    private void Dodge()
    {
        currentDodgeTime += Time.deltaTime;
        int direction = playerBody.velocity.x > 0 ? 1 : -1;
        float anglePerDeltaTime = 360 / dodgeDuration * direction * Time.deltaTime;
        float movePerDeltaTime = dodgeLength / dodgeDuration * direction * Time.deltaTime;
        transform.Translate(new Vector3(movePerDeltaTime, 0.0f, 0.0f), Space.World);
        transform.Rotate(0.0f, 0.0f, -anglePerDeltaTime);
        if (currentDodgeTime >= dodgeDuration)
        {
            state = State.Normal;
            currentDodgeTime = 0;
        }
    }

    private void PlayerShoot()
    {
        shootingTimer -= Time.deltaTime;
        if (Input.GetAxis("Shoot1") > 0.0f && Input.GetAxis("Shoot2") > 0.0f)
        {
            if (shootingTimer <= 0.0f)
            {
                Shoot();
                shootingTimer = shootingInterval;
            }
        }
    }

    private void Shoot()
    {
        GameObject bulletInstance = Instantiate(bulletPrefab);
        bulletInstance.transform.position = gunPlacedPosition.position;
        bulletInstance.transform.SetParent(null);
        Vector3 shootingDirection = gunShootAtPosition.position - gunPlacedPosition.position;
        shootingDirection.Normalize();
        bulletInstance.GetComponent<Rigidbody>().velocity = shootingDirection * bulletSpeed;
        Destroy(bulletInstance, 3);
    }

    private void HoverMotor()
    {
        Ray ray = new Ray(transform.position, -Vector3.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, hoverHeight))
        {
            float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 appliedHoverForce = Vector3.up * proportionalHeight * hoverForce;
            playerBody.AddForce(appliedHoverForce, ForceMode.Acceleration);
        }
    }

    public void LoseHP(int amount)
    {
        this.health -= amount;
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        GameManager.instance.GameOver();
    }
}
