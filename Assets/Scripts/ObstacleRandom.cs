﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ObstacleRandom : MonoBehaviour
{
    GameObject playerController;
    public GameObject Obstacle;

    GameObject floorParent;

    float Difficulty;
    int noOfChilds;
    float probability;


    int targetCount;

    float rand;

    LVLdifficulty DifficultyScript;
    // Start is called before the first frame update
    void Start()
    {
        floorParent = transform.parent.gameObject;
        playerController = GameObject.FindGameObjectWithTag("FloorPlayer");
        DifficultyScript = (LVLdifficulty)playerController.GetComponent<LVLdifficulty>();
        Difficulty = DifficultyScript.LvlDifficulty;
        noOfChilds = transform.childCount;
        probability = (noOfChilds * Difficulty)/noOfChilds;

        targetCount = (int)(noOfChilds * Difficulty);
        
        SpawnObstacles();
        

    }

    void SpawnObstHere(int i)
    {
        GameObject node = transform.GetChild(i).gameObject;
       
        GameObject NewObstacle = Instantiate(Obstacle, node.transform.position + new Vector3(Random.Range(-5,5),0,0), node.transform.rotation);
        Vector3 Dist = NewObstacle.transform.position - NewObstacle.GetComponentInChildren<ObstDestroy>().attachNode.position;
        NewObstacle.transform.Translate(Dist);
        NewObstacle.transform.SetParent(floorParent.transform);
    }

    void SpawnObstacles()
    {
        int i = 0;
        int placed = 0;
        List<int> nonPlacedIndex = new List<int>();
        
        while (i<noOfChilds)
        {
            rand = Random.value;
            if (rand<probability)
            {
                SpawnObstHere(i);
                placed++;
            }
            else
            {
                nonPlacedIndex.Add(i);
            }

            if(placed >= targetCount)
            {
                break;
            }
            i++;
        }
        while(placed < targetCount)
        {
            System.Random rdn = new System.Random();
            int k = rdn.Next(0, nonPlacedIndex.Count);
            SpawnObstHere(nonPlacedIndex[k]);
            nonPlacedIndex.RemoveAt(k);
            placed++;
        }
        
       
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            SpawnObstacles();
        }
    }
}
